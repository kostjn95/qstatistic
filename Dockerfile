FROM openjdk:11
COPY . /usr/q_statistic
WORKDIR /usr/q_statistic
EXPOSE 8080
CMD ["java","-jar","/usr/q_statistic/build/libs/questionnaire_statistic-0.0.1-SNAPSHOT.jar"]


