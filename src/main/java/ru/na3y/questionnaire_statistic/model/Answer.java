package ru.na3y.questionnaire_statistic.model;

import java.util.Objects;

public class Answer {

    private String answer;
    private long count;

    public Answer() {
    }

    public Answer(String answer, long count) {
        this.answer = answer;
        this.count = count;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Answer answer1 = (Answer) o;

        boolean result = answer.equals(answer1.answer);
        return result;
    }

    @Override
    public int hashCode() {
        int result = answer != null ? answer.hashCode() : 0;
        return result;
    }
}
