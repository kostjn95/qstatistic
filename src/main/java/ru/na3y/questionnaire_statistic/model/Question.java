package ru.na3y.questionnaire_statistic.model;

import java.util.Collection;

public class Question {

    private String name;
    private Collection<Answer> answers;

    public Question() {
    }

    public Question(String name, Collection<Answer> answers) {
        this.name = name;
        this.answers = answers;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Collection<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(Collection<Answer> answers) {
        this.answers = answers;
    }
}
