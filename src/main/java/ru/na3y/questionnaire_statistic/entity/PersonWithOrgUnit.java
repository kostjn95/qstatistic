package ru.na3y.questionnaire_statistic.entity;

import javax.persistence.*;

@Entity
@Table(name = "person_with_orgunit_t")
public class PersonWithOrgUnit {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private Long personId;
    private String instituteTitle;

    public PersonWithOrgUnit() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    public String getInstituteTitle() {
        return instituteTitle;
    }

    public void setInstituteTitle(String instituteTitle) {
        this.instituteTitle = instituteTitle;
    }

}
