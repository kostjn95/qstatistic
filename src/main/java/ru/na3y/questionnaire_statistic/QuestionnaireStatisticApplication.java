package ru.na3y.questionnaire_statistic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QuestionnaireStatisticApplication {

	public static void main(String[] args) {
		SpringApplication.run(QuestionnaireStatisticApplication.class, args);
	}

}
