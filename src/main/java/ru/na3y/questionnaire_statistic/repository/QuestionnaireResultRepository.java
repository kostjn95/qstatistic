package ru.na3y.questionnaire_statistic.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import ru.na3y.questionnaire_statistic.entity.QuestionnaireResult;

import java.util.List;

public interface QuestionnaireResultRepository extends CrudRepository<QuestionnaireResult, Long> {

    @Query("SELECT q FROM QuestionnaireResult q " +
            "INNER JOIN PersonWithOrgUnit p on p.personId = q.personId " +
            "WHERE p.instituteTitle in :institute")
    List<QuestionnaireResult> selectQuestionnaireResultByInstitute(@Param("institute") List<String> institute);

}
