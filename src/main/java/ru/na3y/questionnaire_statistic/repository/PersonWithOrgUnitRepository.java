package ru.na3y.questionnaire_statistic.repository;

import org.springframework.data.repository.CrudRepository;
import ru.na3y.questionnaire_statistic.entity.PersonWithOrgUnit;

public interface PersonWithOrgUnitRepository extends CrudRepository<PersonWithOrgUnit, Long> {

    PersonWithOrgUnit findByPersonId(long personId);

}
