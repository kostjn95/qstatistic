package ru.na3y.questionnaire_statistic.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Controller
public class FrontendController {

    @GetMapping("/")
    public String index() {
        return "index";
    }

    @GetMapping("/upload")
    public String upload() {
        return "upload";
    }

    @PostMapping("/uploadResultFile")
    public List<String> uploadResultFile(@RequestParam("file") MultipartFile file) {
        // TODO Логика добваление данных в БД
        return null;
    }

    @PostMapping("/uploadStudentsWithInstituteFile")
    public List<String> uploadStudentsWithInstituteFile(@RequestParam("file") MultipartFile file) {
        // TODO Логика добваление данных в БД
        return null;
    }

}
