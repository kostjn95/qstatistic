package ru.na3y.questionnaire_statistic.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.na3y.questionnaire_statistic.entity.QuestionnaireResult;
import ru.na3y.questionnaire_statistic.model.Answer;
import ru.na3y.questionnaire_statistic.model.Question;
import ru.na3y.questionnaire_statistic.repository.QuestionnaireResultRepository;

import java.util.*;
import java.util.stream.Collectors;

@RestController
public class QuestionnaireResultController {

    Logger logger = LoggerFactory.getLogger(QuestionnaireResultController.class);

    @Autowired
    private QuestionnaireResultRepository questionnaireResultRepository;

    @GetMapping("/questionnaireResultsByInstitute")
    public List<Question> getQuestionnaireResultsByInstitute(
            @RequestParam(value = "institute", defaultValue = "Институт международного образования") String institute
    ) {
        logger.info("questionnaireResultsByInstitute - institute: " + institute);

        List<String> paramInstitute = new ArrayList<>();
        paramInstitute.add(institute);
        if ("Институт технологий управления".equals(institute)) {
            paramInstitute.add("Институт инновационных технологий и государственного управления");
            paramInstitute.add("Институт экономики и права");
        }
        List<QuestionnaireResult> questionnaireResults = questionnaireResultRepository.selectQuestionnaireResultByInstitute(paramInstitute);

        List<Question> result = new ArrayList<>();

        Set<Answer> a1 = new HashSet<>();
        for (String a : questionnaireResults.stream().map(QuestionnaireResult::getAnswer1).distinct().collect(Collectors.toList())) {
            if (a.equals("")) {
                continue;
            }
            a1.add(
                    new Answer(
                            a,
                            questionnaireResults.stream()
                                    .map(QuestionnaireResult::getAnswer1)
                                    .filter(v -> v.equals(a))
                                    .count()
                    )
            );
        }
        initAnswers(a1, 1);
        Question q1 = new Question("1. Удовлетворены ли Вы в целом обучением в Университете?", a1);
        result.add(q1);

        Set<Answer> a2 = new HashSet<>();
        for (String a : questionnaireResults.stream().map(QuestionnaireResult::getAnswer2).distinct().collect(Collectors.toList())) {
            if (a.equals("")) {
                continue;
            }
            a2.add(
                    new Answer(
                            a,
                            questionnaireResults.stream()
                                    .map(QuestionnaireResult::getAnswer2)
                                    .filter(v -> v.equals(a))
                                    .count()
                    )
            );
        }
        initAnswers(a2, 2);
        Question q2 = new Question("2. Удовлетворены ли Вы качеством информирования обучающихся по вопросам учебного процесса в Университете?", a2);
        result.add(q2);

        Set<Answer> a3 = new HashSet<>();
        for (String a : questionnaireResults.stream().map(QuestionnaireResult::getAnswer3).distinct().collect(Collectors.toList())) {
            if (a.equals("")) {
                continue;
            }
            a3.add(
                    new Answer(
                            a,
                            questionnaireResults.stream()
                                    .map(QuestionnaireResult::getAnswer3)
                                    .filter(v -> v.equals(a))
                                    .count()
                    )
            );
        }
        initAnswers(a3, 3);
        Question q3 = new Question("3. С учетом Вашего ответа на предыдущий вопрос (вопрос 2) выделите основные характеристики организации информирования обучающихся в Университете", a3);
        result.add(q3);

        Set<Answer> a4 = new HashSet<>();
        for (String a : questionnaireResults.stream().map(QuestionnaireResult::getAnswer4).distinct().collect(Collectors.toList())) {
            if (a.equals("")) {
                continue;
            }
            a4.add(
                    new Answer(
                            a,
                            questionnaireResults.stream()
                                    .map(QuestionnaireResult::getAnswer4)
                                    .filter(v -> v.equals(a))
                                    .count()
                    )
            );
        }
        initAnswers(a4, 4);
        Question q4 = new Question("4. Какие общедоступные информационные ресурсы Вы используете для получения информации по вопросам учебного процесса в Университете", a4);
        result.add(q4);

        Set<Answer> a5 = new HashSet<>();
        for (String a : questionnaireResults.stream().map(QuestionnaireResult::getAnswer5).distinct().collect(Collectors.toList())) {
            if (a.equals("")) {
                continue;
            }
            a5.add(
                    new Answer(
                            a,
                            questionnaireResults.stream()
                                    .map(QuestionnaireResult::getAnswer5)
                                    .filter(v -> v.equals(a))
                                    .count()
                    )
            );
        }
        initAnswers(a5, 5);
        Question q5 = new Question("5. Насколько Вы удовлетворены качеством информирования обучающихся по вопросам учебного процесса, осуществляемого с использованием официального сайта Университета", a5);
        result.add(q5);

        Set<Answer> a6 = new HashSet<>();
        for (String a : questionnaireResults.stream().map(QuestionnaireResult::getAnswer6).distinct().collect(Collectors.toList())) {
            if (a.equals("")) {
                continue;
            }
            a6.add(
                    new Answer(
                            a,
                            questionnaireResults.stream()
                                    .map(QuestionnaireResult::getAnswer6)
                                    .filter(v -> v.equals(a))
                                    .count()
                    )
            );
        }
        initAnswers(a6, 61);
        Question q6 = new Question("6.1. Насколько Вы удовлетворены наличием и функционированием на сайте Университета информации о дистанционных способах обратной связи и взаимодействия с обучающимися. Сведения о контактных телефонах", a6);
        result.add(q6);

        Set<Answer> a7 = new HashSet<>();
        for (String a : questionnaireResults.stream().map(QuestionnaireResult::getAnswer7).distinct().collect(Collectors.toList())) {
            if (a.equals("")) {
                continue;
            }
            a7.add(
                    new Answer(
                            a,
                            questionnaireResults.stream()
                                    .map(QuestionnaireResult::getAnswer7)
                                    .filter(v -> v.equals(a))
                                    .count()
                    )
            );
        }
        initAnswers(a7, 62);
        Question q7 = new Question("6.2. Насколько Вы удовлетворены наличием и функционированием на сайте Университета информации о дистанционных способах обратной связи и взаимодействия с обучающимися. Сведения об адресах электронной почты", a7);
        result.add(q7);

        Set<Answer> a8 = new HashSet<>();
        for (String a : questionnaireResults.stream().map(QuestionnaireResult::getAnswer8).distinct().collect(Collectors.toList())) {
            if (a.equals("")) {
                continue;
            }
            a8.add(
                    new Answer(
                            a,
                            questionnaireResults.stream()
                                    .map(QuestionnaireResult::getAnswer8)
                                    .filter(v -> v.equals(a))
                                    .count()
                    )
            );
        }
        initAnswers(a8, 63);
        Question q8 = new Question("6.3 Насколько Вы удовлетворены наличием и функционированием на сайте Университета информации о дистанционных способах обратной связи и взаимодействия с обучающимися. Сведения об электронных сервисах (форма для подачи электронного обращения (жалобы, предложение), получении консультации по оказываемым услугах и иных", a8);
        result.add(q8);

        Set<Answer> a9 = new HashSet<>();
        for (String a : questionnaireResults.stream().map(QuestionnaireResult::getAnswer9).distinct().collect(Collectors.toList())) {
            if (a.equals("")) {
                continue;
            }
            a9.add(
                    new Answer(
                            a,
                            questionnaireResults.stream()
                                    .map(QuestionnaireResult::getAnswer9)
                                    .filter(v -> v.equals(a))
                                    .count()
                    )
            );
        }
        initAnswers(a9, 64);
        Question q9 = new Question("6.4. Насколько Вы удовлетворены наличием и функционированием на сайте Университета информации о дистанционных способах обратной связи и взаимодействия с обучающимися. Раздел «Часто задаваемые вопросы»", a9);
        result.add(q9);

        Set<Answer> a10 = new HashSet<>();
        for (String a : questionnaireResults.stream().map(QuestionnaireResult::getAnswer10).distinct().collect(Collectors.toList())) {
            if (a.equals("")) {
                continue;
            }
            a10.add(
                    new Answer(
                            a,
                            questionnaireResults.stream()
                                    .map(QuestionnaireResult::getAnswer10)
                                    .filter(v -> v.equals(a))
                                    .count()
                    )
            );
        }
        initAnswers(a10, 65);
        Question q10 = new Question("6.5. Насколько Вы удовлетворены наличием и функционированием на сайте Университета информации о дистанционных способах обратной связи и взаимодействия с обучающимися. Техническая возможность выражения обучающимся мнения о качестве условия оказания образовательных услуг (наличие анкеты для обучающихся или гиперссылка на нее)", a10);
        result.add(q10);

        Set<Answer> a11 = new HashSet<>();
        for (String a : questionnaireResults.stream().map(QuestionnaireResult::getAnswer11).distinct().collect(Collectors.toList())) {
            if (a.equals("")) {
                continue;
            }
            a11.add(
                    new Answer(
                            a,
                            questionnaireResults.stream()
                                    .map(QuestionnaireResult::getAnswer11)
                                    .filter(v -> v.equals(a))
                                    .count()
                    )
            );
        }
        initAnswers(a11, 7);
        Question q11 = new Question("7. Удовлетворяет ли Вас комфортность условий предоставления образовательных услуг в Университете", a11);
        result.add(q11);

        Set<Answer> a12 = new HashSet<>();
        for (String a : questionnaireResults.stream().map(QuestionnaireResult::getAnswer12).distinct().collect(Collectors.toList())) {
            if (a.equals("")) {
                continue;
            }
            a12.add(
                    new Answer(
                            a,
                            questionnaireResults.stream()
                                    .map(QuestionnaireResult::getAnswer12)
                                    .filter(v -> v.equals(a))
                                    .count()
                    )
            );
        }
        initAnswers(a12, 81);
        Question q12 = new Question("8.1. Насколько Вы удовлетворены составляющими комфортности основных условий предоставления образовательных услуг в Университете. Наличие комфортной зоны отдыха, оборудованной соответствующей мебелью", a12);
        result.add(q12);

        Set<Answer> a13 = new HashSet<>();
        for (String a : questionnaireResults.stream().map(QuestionnaireResult::getAnswer13).distinct().collect(Collectors.toList())) {
            if (a.equals("")) {
                continue;
            }
            a13.add(
                    new Answer(
                            a,
                            questionnaireResults.stream()
                                    .map(QuestionnaireResult::getAnswer13)
                                    .filter(v -> v.equals(a))
                                    .count()
                    )
            );
        }
        initAnswers(a13, 82);
        Question q13 = new Question("8.2. Насколько Вы удовлетворены составляющими комфортности основных условий предоставления образовательных услуг в Университете. Наличие понятной навигации внутри Университета", a13);
        result.add(q13);

        Set<Answer> a14 = new HashSet<>();
        for (String a : questionnaireResults.stream().map(QuestionnaireResult::getAnswer14).distinct().collect(Collectors.toList())) {
            if (a.equals("")) {
                continue;
            }
            a14.add(
                    new Answer(
                            a,
                            questionnaireResults.stream()
                                    .map(QuestionnaireResult::getAnswer14)
                                    .filter(v -> v.equals(a))
                                    .count()
                    )
            );
        }
        initAnswers(a14, 83);
        Question q14 = new Question("8.3. Насколько Вы удовлетворены составляющими комфортности основных условий предоставления образовательных услуг в Университете. Наличие и доступность источников питьевой воды:", a14);
        result.add(q14);

        Set<Answer> a15 = new HashSet<>();
        for (String a : questionnaireResults.stream().map(QuestionnaireResult::getAnswer15).distinct().collect(Collectors.toList())) {
            if (a.equals("")) {
                continue;
            }
            a15.add(
                    new Answer(
                            a,
                            questionnaireResults.stream()
                                    .map(QuestionnaireResult::getAnswer15)
                                    .filter(v -> v.equals(a))
                                    .count()
                    )
            );
        }
        initAnswers(a15, 84);
        Question q15 = new Question("8.4 Насколько Вы удовлетворены составляющими комфортности основных условий предоставления образовательных услуг в Университете. Наличие и доступность санитарно-гигиенических помещений", a15);
        result.add(q15);

        Set<Answer> a16 = new HashSet<>();
        for (String a : questionnaireResults.stream().map(QuestionnaireResult::getAnswer16).distinct().collect(Collectors.toList())) {
            if (a.equals("")) {
                continue;
            }
            a16.add(
                    new Answer(
                            a,
                            questionnaireResults.stream()
                                    .map(QuestionnaireResult::getAnswer16)
                                    .filter(v -> v.equals(a))
                                    .count()
                    )
            );
        }
        initAnswers(a16, 85);
        Question q16 = new Question("8.5. Насколько Вы удовлетворены составляющими комфортности основных условий предоставления образовательных услуг в Университете. Санитарное состояние помещений Университета", a16);
        result.add(q16);

        Set<Answer> a17 = new HashSet<>();
        for (String a : questionnaireResults.stream().map(QuestionnaireResult::getAnswer17).distinct().collect(Collectors.toList())) {
            if (a.equals("")) {
                continue;
            }
            a17.add(
                    new Answer(
                            a,
                            questionnaireResults.stream()
                                    .map(QuestionnaireResult::getAnswer17)
                                    .filter(v -> v.equals(a))
                                    .count()
                    )
            );
        }
        initAnswers(a17, 86);
        Question q17 = new Question("8.6. Насколько Вы удовлетворены составляющими комфортности основных условий предоставления образовательных услуг в Университете. Транспортная доступность Университета (возможность доехать до Университета на общественном транспорте)", a17);
        result.add(q17);

        Set<Answer> a18 = new HashSet<>();
        for (String a : questionnaireResults.stream().map(QuestionnaireResult::getAnswer18).distinct().collect(Collectors.toList())) {
            if (a.equals("")) {
                continue;
            }
            a18.add(
                    new Answer(
                            a,
                            questionnaireResults.stream()
                                    .map(QuestionnaireResult::getAnswer18)
                                    .filter(v -> v.equals(a))
                                    .count()
                    )
            );
        }
        initAnswers(a18, 9);
        Question q18 = new Question("9. Удовлетворены ли Вы доступностью образовательных услуг для инвалидов в Университете?", a18);
        result.add(q18);

        Set<Answer> a19 = new HashSet<>();
        for (String a : questionnaireResults.stream().map(QuestionnaireResult::getAnswer19).distinct().collect(Collectors.toList())) {
            if (a.equals("")) {
                continue;
            }
            a19.add(
                    new Answer(
                            a,
                            questionnaireResults.stream()
                                    .map(QuestionnaireResult::getAnswer19)
                                    .filter(v -> v.equals(a))
                                    .count()
                    )
            );
        }
        initAnswers(a19, 101);
        Question q19 = new Question("10.1. Насколько Вы удовлетворены оборудованием помещений Университета и прилегающих к нему территорий с учетом доступности для инвалидов. Оборудование водных групп пандусами (подъемными платформами)", a19);
        result.add(q19);

        Set<Answer> a20 = new HashSet<>();
        for (String a : questionnaireResults.stream().map(QuestionnaireResult::getAnswer20).distinct().collect(Collectors.toList())) {
            if (a.equals("")) {
                continue;
            }
            a20.add(
                    new Answer(
                            a,
                            questionnaireResults.stream()
                                    .map(QuestionnaireResult::getAnswer20)
                                    .filter(v -> v.equals(a))
                                    .count()
                    )
            );
        }
        initAnswers(a20, 102);
        Question q20 = new Question("10.2. Насколько Вы удовлетворены оборудованием помещений Университета и прилегающих к нему территорий с учетом доступности для инвалидов. Наличие выделенных стоянок для автотранспортных средств инвалидов", a20);
        result.add(q20);

        Set<Answer> a21 = new HashSet<>();
        for (String a : questionnaireResults.stream().map(QuestionnaireResult::getAnswer21).distinct().collect(Collectors.toList())) {
            if (a.equals("")) {
                continue;
            }
            a21.add(
                    new Answer(
                            a,
                            questionnaireResults.stream()
                                    .map(QuestionnaireResult::getAnswer21)
                                    .filter(v -> v.equals(a))
                                    .count()
                    )
            );
        }
        initAnswers(a21, 103);
        Question q21 = new Question("10.3. Насколько Вы удовлетворены оборудованием помещений Университета и прилегающих к нему территорий с учетом доступности для инвалидов. Наличие адаптированных лифтов, поручней, расширенных дверных проемов", a21);
        result.add(q21);

        Set<Answer> a22 = new HashSet<>();
        for (String a : questionnaireResults.stream().map(QuestionnaireResult::getAnswer22).distinct().collect(Collectors.toList())) {
            if (a.equals("")) {
                continue;
            }
            a22.add(
                    new Answer(
                            a,
                            questionnaireResults.stream()
                                    .map(QuestionnaireResult::getAnswer22)
                                    .filter(v -> v.equals(a))
                                    .count()
                    )
            );
        }
        initAnswers(a22, 104);
        Question q22 = new Question("10.4. Насколько Вы удовлетворены оборудованием помещений Университета и прилегающих к нему территорий с учетом доступности для инвалидов. Наличие сменных кресел-стоянок", a22);
        result.add(q22);

        Set<Answer> a23 = new HashSet<>();
        for (String a : questionnaireResults.stream().map(QuestionnaireResult::getAnswer23).distinct().collect(Collectors.toList())) {
            if (a.equals("")) {
                continue;
            }
            a23.add(
                    new Answer(
                            a,
                            questionnaireResults.stream()
                                    .map(QuestionnaireResult::getAnswer23)
                                    .filter(v -> v.equals(a))
                                    .count()
                    )
            );
        }
        initAnswers(a23, 105);
        Question q23 = new Question("10.5. Насколько Вы удовлетворены оборудованием помещений Университета и прилегающих к нему территорий с учетом доступности для инвалидов. Наличие специально оборудованных санитарно-гигиенических помещений", a23);
        result.add(q23);

        Set<Answer> a24 = new HashSet<>();
        for (String a : questionnaireResults.stream().map(QuestionnaireResult::getAnswer24).distinct().collect(Collectors.toList())) {
            if (a.equals("")) {
                continue;
            }
            a24.add(
                    new Answer(
                            a,
                            questionnaireResults.stream()
                                    .map(QuestionnaireResult::getAnswer24)
                                    .filter(v -> v.equals(a))
                                    .count()
                    )
            );
        }
        initAnswers(a24, 111);
        Question q24 = new Question("11.1. Насколько Вы удовлетворены обеспечением в Университете условий доступности, позволяющих инвалидам получать образовательные услуги наравне с другими. Дублирование для инвалидов по слуху и зрению звуковой и зрительной информации", a24);
        result.add(q24);

        Set<Answer> a25 = new HashSet<>();
        for (String a : questionnaireResults.stream().map(QuestionnaireResult::getAnswer25).distinct().collect(Collectors.toList())) {
            if (a.equals("")) {
                continue;
            }
            a25.add(
                    new Answer(
                            a,
                            questionnaireResults.stream()
                                    .map(QuestionnaireResult::getAnswer25)
                                    .filter(v -> v.equals(a))
                                    .count()
                    )
            );
        }
        initAnswers(a25, 112);
        Question q25 = new Question("11.2. Насколько Вы удовлетворены обеспечением в Университете условий доступности, позволяющих инвалидам получать образовательные услуги наравне с другими. Дублирование надписей, знаков и иной текстовой и графической информации знаками, выполненными рельефно-точечным шрифтом Брайля", a25);
        result.add(q25);

        Set<Answer> a26 = new HashSet<>();
        for (String a : questionnaireResults.stream().map(QuestionnaireResult::getAnswer26).distinct().collect(Collectors.toList())) {
            if (a.equals("")) {
                continue;
            }
            a26.add(
                    new Answer(
                            a,
                            questionnaireResults.stream()
                                    .map(QuestionnaireResult::getAnswer26)
                                    .filter(v -> v.equals(a))
                                    .count()
                    )
            );
        }
        initAnswers(a26, 113);
        Question q26 = new Question("11.3. Насколько Вы удовлетворены обеспечением в Университете условий доступности, позволяющих инвалидам получать образовательные услуги наравне с другими. Возможность предоставления инвалидам по слуху (слуху и зрению) услуг сурдопереводчика (тифлосурдопереводчика)", a26);
        result.add(q26);

        Set<Answer> a27 = new HashSet<>();
        for (String a : questionnaireResults.stream().map(QuestionnaireResult::getAnswer27).distinct().collect(Collectors.toList())) {
            if (a.equals("")) {
                continue;
            }
            a27.add(
                    new Answer(
                            a,
                            questionnaireResults.stream()
                                    .map(QuestionnaireResult::getAnswer27)
                                    .filter(v -> v.equals(a))
                                    .count()
                    )
            );
        }
        initAnswers(a27, 114);
        Question q27 = new Question("11.4. Насколько Вы удовлетворены обеспечением в Университете условий доступности, позволяющих инвалидам получать образовательные услуги наравне с другими. Наличие альтернативной версии сайта для инвалидов по зрению", a27);
        result.add(q27);

        Set<Answer> a28 = new HashSet<>();
        for (String a : questionnaireResults.stream().map(QuestionnaireResult::getAnswer28).distinct().collect(Collectors.toList())) {
            if (a.equals("")) {
                continue;
            }
            a28.add(
                    new Answer(
                            a,
                            questionnaireResults.stream()
                                    .map(QuestionnaireResult::getAnswer28)
                                    .filter(v -> v.equals(a))
                                    .count()
                    )
            );
        }
        initAnswers(a28, 115);
        Question q28 = new Question("11.5. Насколько Вы удовлетворены обеспечением в Университете условий доступности, позволяющих инвалидам получать образовательные услуги наравне с другими. Помощь, оказываемая работниками Университета, прошедшими необходимое обучение (инструктирование) по сопровождению инвалидов в помещениях организации и на прилегающей территории", a28);
        result.add(q28);

        Set<Answer> a29 = new HashSet<>();
        for (String a : questionnaireResults.stream().map(QuestionnaireResult::getAnswer29).distinct().collect(Collectors.toList())) {
            if (a.equals("")) {
                continue;
            }
            a29.add(
                    new Answer(
                            a,
                            questionnaireResults.stream()
                                    .map(QuestionnaireResult::getAnswer29)
                                    .filter(v -> v.equals(a))
                                    .count()
                    )
            );
        }
        initAnswers(a29, 116);
        Question q29 = new Question("11.6. Насколько Вы удовлетворены обеспечением в Университете условий доступности, позволяющих инвалидам получать образовательные услуги наравне с другими. Наличие возможности предоставления образовательных услуг в дистанционном режиме или на дому", a29);
        result.add(q29);

        Set<Answer> a30 = new HashSet<>();
        for (String a : questionnaireResults.stream().map(QuestionnaireResult::getAnswer30).distinct().collect(Collectors.toList())) {
            if (a.equals("")) {
                continue;
            }
            a30.add(
                    new Answer(
                            a,
                            questionnaireResults.stream()
                                    .map(QuestionnaireResult::getAnswer30)
                                    .filter(v -> v.equals(a))
                                    .count()
                    )
            );
        }
        initAnswers(a30, 121);
        Question q30 = new Question("12.1. Удовлетворены ли Вы доброжелательностью, вежливостью работников Университета. Обеспечивающих первичный контакт и информирование получателя услуги при непосредственном обращении в организацию (работники приемной комиссии, секретариата, учебной части и пр.)", a30);
        result.add(q30);

        Set<Answer> a31 = new HashSet<>();
        for (String a : questionnaireResults.stream().map(QuestionnaireResult::getAnswer31).distinct().collect(Collectors.toList())) {
            if (a.equals("")) {
                continue;
            }
            a31.add(
                    new Answer(
                            a,
                            questionnaireResults.stream()
                                    .map(QuestionnaireResult::getAnswer31)
                                    .filter(v -> v.equals(a))
                                    .count()
                    )
            );
        }
        initAnswers(a31, 122);
        Question q31 = new Question("12.2. Удовлетворены ли Вы доброжелательностью, вежливостью работников Университета. Обеспечивающих непосредственное оказание услуги при обращении в Университет", a31);
        result.add(q31);

        Set<Answer> a32 = new HashSet<>();
        for (String a : questionnaireResults.stream().map(QuestionnaireResult::getAnswer32).distinct().collect(Collectors.toList())) {
            if (a.equals("")) {
                continue;
            }
            a32.add(
                    new Answer(
                            a,
                            questionnaireResults.stream()
                                    .map(QuestionnaireResult::getAnswer32)
                                    .filter(v -> v.equals(a))
                                    .count()
                    )
            );
        }
        initAnswers(a32, 123);
        Question q32 = new Question("12.3. Удовлетворены ли Вы доброжелательностью, вежливостью работников Университета. При использовании дистанционных форм взаимодействия", a32);
        result.add(q32);

        Set<Answer> a33 = new HashSet<>();
        for (String a : questionnaireResults.stream().map(QuestionnaireResult::getAnswer33).distinct().collect(Collectors.toList())) {
            if (a.equals("")) {
                continue;
            }
            a33.add(
                    new Answer(
                            a,
                            questionnaireResults.stream()
                                    .map(QuestionnaireResult::getAnswer33)
                                    .filter(v -> v.equals(a))
                                    .count()
                    )
            );
        }
        initAnswers(a33, 124);
        Question q33 = new Question("12.4 Удовлетворены ли Вы доброжелательностью, вежливостью работников Университета. Подразделений, обеспечивающих организацию образовательного процесса (кафедр, деканата, учебного отдела и др.)", a33);
        result.add(q33);

        Set<Answer> a34 = new HashSet<>();
        for (String a : questionnaireResults.stream().map(QuestionnaireResult::getAnswer34).distinct().collect(Collectors.toList())) {
            if (a.equals("")) {
                continue;
            }
            a34.add(
                    new Answer(
                            a,
                            questionnaireResults.stream()
                                    .map(QuestionnaireResult::getAnswer34)
                                    .filter(v -> v.equals(a))
                                    .count()
                    )
            );
        }
        initAnswers(a34, 125);
        Question q34 = new Question("12.5. Удовлетворены ли Вы доброжелательностью, вежливостью работников Университета. Функциональных структурных подразделений Университета, в которые Вы обращаетесь при необходимости (Бухгалтерия, Управление делами, Служба безопасности, пр.)", a34);
        result.add(q34);

        Set<Answer> a35 = new HashSet<>();
        for (String a : questionnaireResults.stream().map(QuestionnaireResult::getAnswer35).distinct().collect(Collectors.toList())) {
            if (a.equals("")) {
                continue;
            }
            a35.add(
                    new Answer(
                            a,
                            questionnaireResults.stream()
                                    .map(QuestionnaireResult::getAnswer35)
                                    .filter(v -> v.equals(a))
                                    .count()
                    )
            );
        }
        initAnswers(a35, 131);
        Question q35 = new Question("13.1. Насколько Вы удовлетворены основными характеристиками работы персонала, структурных подразделений Университета, в которые Вы обращаетесь при необходимости (Бухгалтерия, Управление делами, Служба безопасности, пр.). Доступность (удобные графики работы)", a35);
        result.add(q35);

        Set<Answer> a36 = new HashSet<>();
        for (String a : questionnaireResults.stream().map(QuestionnaireResult::getAnswer36).distinct().collect(Collectors.toList())) {
            if (a.equals("")) {
                continue;
            }
            a36.add(
                    new Answer(
                            a,
                            questionnaireResults.stream()
                                    .map(QuestionnaireResult::getAnswer36)
                                    .filter(v -> v.equals(a))
                                    .count()
                    )
            );
        }
        initAnswers(a36, 132);
        Question q36 = new Question("13.2. Насколько Вы удовлетворены основными характеристиками работы персонала, структурных подразделений Университета, в которые Вы обращаетесь при необходимости (Бухгалтерия, Управление делами, Служба безопасности, пр.). Вежливость и доброжелательность сотрудников", a36);
        result.add(q36);

        Set<Answer> a37 = new HashSet<>();
        for (String a : questionnaireResults.stream().map(QuestionnaireResult::getAnswer37).distinct().collect(Collectors.toList())) {
            if (a.equals("")) {
                continue;
            }
            a37.add(
                    new Answer(
                            a,
                            questionnaireResults.stream()
                                    .map(QuestionnaireResult::getAnswer37)
                                    .filter(v -> v.equals(a))
                                    .count()
                    )
            );
        }
        initAnswers(a37, 133);
        Question q37 = new Question("13.3. Насколько Вы удовлетворены основными характеристиками работы персонала, структурных подразделений Университета, в которые Вы обращаетесь при необходимости (Бухгалтерия, Управление делами, Служба безопасности, пр.). Уважительное отношение к обучающемуся", a37);
        result.add(q37);

        Set<Answer> a38 = new HashSet<>();
        for (String a : questionnaireResults.stream().map(QuestionnaireResult::getAnswer38).distinct().collect(Collectors.toList())) {
            if (a.equals("")) {
                continue;
            }
            a38.add(
                    new Answer(
                            a,
                            questionnaireResults.stream()
                                    .map(QuestionnaireResult::getAnswer38)
                                    .filter(v -> v.equals(a))
                                    .count()
                    )
            );
        }
        initAnswers(a38, 134);
        Question q38 = new Question("13.4. Насколько Вы удовлетворены основными характеристиками работы персонала, структурных подразделений Университета, в которые Вы обращаетесь при необходимости (Бухгалтерия, Управление делами, Служба безопасности, пр.). Оперативность решения вопроса", a38);
        result.add(q38);

        Set<Answer> a39 = new HashSet<>();
        for (String a : questionnaireResults.stream().map(QuestionnaireResult::getAnswer39).distinct().collect(Collectors.toList())) {
            if (a.equals("")) {
                continue;
            }
            a39.add(
                    new Answer(
                            a,
                            questionnaireResults.stream()
                                    .map(QuestionnaireResult::getAnswer39)
                                    .filter(v -> v.equals(a))
                                    .count()
                    )
            );
        }
        initAnswers(a39, 14);
        Question q39 = new Question("14. Удовлетворены ли Вы организационными условиями предоставления образовательных услуг в Университете", a39);
        result.add(q39);

        Set<Answer> a40 = new HashSet<>();
        for (String a : questionnaireResults.stream().map(QuestionnaireResult::getAnswer40).distinct().collect(Collectors.toList())) {
            if (a.equals("")) {
                continue;
            }
            a40.add(
                    new Answer(
                            a,
                            questionnaireResults.stream()
                                    .map(QuestionnaireResult::getAnswer40)
                                    .filter(v -> v.equals(a))
                                    .count()
                    )
            );
        }
        initAnswers(a40, 15);
        Question q40 = new Question("15. Готовы ли Вы рекомендовать Университет родственникам и знакомым для получения образования?", a40);
        result.add(q40);

        Set<Answer> a41 = new HashSet<>();
        for (String a : questionnaireResults.stream().map(QuestionnaireResult::getAnswer41).distinct().collect(Collectors.toList())) {
            if (a.equals("")) {
                continue;
            }
            a41.add(
                    new Answer(
                            a,
                            questionnaireResults.stream()
                                    .map(QuestionnaireResult::getAnswer41)
                                    .filter(v -> v.equals(a))
                                    .count()
                    )
            );
        }
        initAnswers(a41, 16);
        Question q41 = new Question("16. Если бы Вы снова выбирали образовательную организацию для получения образования, выбрали ли Вы Университет", a41);
        result.add(q41);

        Set<Answer> a42 = new HashSet<>();
        for (String a : questionnaireResults.stream().map(QuestionnaireResult::getAnswer42).distinct().collect(Collectors.toList())) {
            if (a.equals("")) {
                continue;
            }
            a42.add(
                    new Answer(
                            a,
                            questionnaireResults.stream()
                                    .map(QuestionnaireResult::getAnswer42)
                                    .filter(v -> v.equals(a))
                                    .count()
                    )
            );
        }
        initAnswers(a42, 17);
        Question q42 = new Question("17. Как успешно Вы сдали зимнюю сессию 2019-2020 учебного года?", a42);
        result.add(q42);

        Set<Answer> a43 = new HashSet<>();
        for (String a : questionnaireResults.stream().map(QuestionnaireResult::getAnswer43).distinct().collect(Collectors.toList())) {
            if (a.equals("")) {
                continue;
            }
            a43.add(
                    new Answer(
                            a,
                            questionnaireResults.stream()
                                    .map(QuestionnaireResult::getAnswer43)
                                    .filter(v -> v.equals(a))
                                    .count()
                    )
            );
        }
        initAnswers(a43, 18);
        Question q43 = new Question("18. Если Вы сдали все, то какие оценки Вы получили в зимнюю сессию 2019-2020 учебного года?", a43);
        result.add(q43);

        Set<Answer> a44 = new HashSet<>();
        for (String a : questionnaireResults.stream().map(QuestionnaireResult::getAnswer44).distinct().collect(Collectors.toList())) {
            if (a.equals("")) {
                continue;
            }
            a44.add(
                    new Answer(
                            a,
                            questionnaireResults.stream()
                                    .map(QuestionnaireResult::getAnswer44)
                                    .filter(v -> v.equals(a))
                                    .count()
                    )
            );
        }
        initAnswers(a44, 19);
        Question q44 = new Question("19. Ваш пол?", a44);
        result.add(q44);

        Set<Answer> a45 = new HashSet<>();
        for (String a : questionnaireResults.stream().map(QuestionnaireResult::getAnswer45).distinct().collect(Collectors.toList())) {
            if (a.equals("")) {
                continue;
            }
            a45.add(
                    new Answer(
                            a,
                            questionnaireResults.stream()
                                    .map(QuestionnaireResult::getAnswer45)
                                    .filter(v -> v.equals(a))
                                    .count()
                    )
            );
        }
        initAnswers(a45, 20);
        Question q45 = new Question("20. Ваш возраст?", a45);
        result.add(q45);

        Set<Answer> a46 = new HashSet<>();
        for (String a : questionnaireResults.stream().map(QuestionnaireResult::getAnswer46).distinct().collect(Collectors.toList())) {
            if (a.equals("")) {
                continue;
            }
            a46.add(
                    new Answer(
                            a,
                            questionnaireResults.stream()
                                    .map(QuestionnaireResult::getAnswer46)
                                    .filter(v -> v.equals(a))
                                    .count()
                    )
            );
        }
        initAnswers(a46, 21);
        Question q46 = new Question("21. Курс, на котором Вы учитесь?", a46);
        result.add(q46);

        Set<Answer> a47 = new HashSet<>();
        for (String a : questionnaireResults.stream().map(QuestionnaireResult::getAnswer47).distinct().collect(Collectors.toList())) {
            if (a.equals("")) {
                continue;
            }
            a47.add(
                    new Answer(
                            a,
                            questionnaireResults.stream()
                                    .map(QuestionnaireResult::getAnswer47)
                                    .filter(v -> v.equals(a))
                                    .count()
                    )
            );
        }
        initAnswers(a47, 22);
        Question q47 = new Question("22. Уровень высшего профессионального образования, на котором Вы обучаетесь", a47);
        result.add(q47);

        Set<Answer> a48 = new HashSet<>();
        for (String a : questionnaireResults.stream().map(QuestionnaireResult::getAnswer48).distinct().collect(Collectors.toList())) {
            if (a.equals("")) {
                continue;
            }
            a48.add(
                    new Answer(
                            a,
                            questionnaireResults.stream()
                                    .map(QuestionnaireResult::getAnswer48)
                                    .filter(v -> v.equals(a))
                                    .count()
                    )
            );
        }
        initAnswers(a48, 23);
        Question q48 = new Question("23. Где Вы проживаете", a48);
        result.add(q48);

        return result;
    }

    private void initAnswers(Set<Answer> result, int questionNumber) {
        switch (questionNumber) {
            case 3: {
                result.add(new Answer("полнота и открытость информации", 0));
                result.add(new Answer("четкость и понятность информации", 0));
                result.add(new Answer("своевременность информации", 0));
                result.add(new Answer("легкость поиска информации", 0));
                result.add(new Answer("недостаточность информации", 0));
                result.add(new Answer("нечеткость информации", 0));
                result.add(new Answer("несвоевременность информации", 0));
                result.add(new Answer("трудности в поиске нужной информации", 0));
                break;
            }
            case 4: {
                result.add(new Answer("информационные стенды в помещении Университета", 0));
                result.add(new Answer("официальный сайт Университета в информационно-телекоммуникационной сети «Интернет»", 0));
                break;
            }
            case 7: {
                result.add(new Answer("полностью удовлетворяет", 0));
                result.add(new Answer("частично удовлетворяет", 0));
                result.add(new Answer("не удовлетворяет", 0));
                result.add(new Answer("затрудняюсь ответить", 0));
                break;
            }
            case 15:
            case 16: {
                result.add(new Answer("да", 0));
                result.add(new Answer("нет", 0));
                result.add(new Answer("затрудняюсь ответить", 0));
                break;
            }
            case 17: {
                result.add(new Answer("Сдал (а) все", 0));
                result.add(new Answer("Имею задолженности", 0));
                break;
            }
            case 18: {
                result.add(new Answer("Отлично", 0));
                result.add(new Answer("Хорошо и отлично", 0));
                result.add(new Answer("Хорошо", 0));
                result.add(new Answer("Хорошо и удовлетворительно", 0));
                result.add(new Answer("Удовлетворительно", 0));
                break;
            }
            case 19: {
                result.add(new Answer("мужской", 0));
                result.add(new Answer("женский", 0));
                break;
            }
            case 20: {
                result.add(new Answer("17 – 19 лет", 0));
                result.add(new Answer("20 – 22 года", 0));
                result.add(new Answer("23 – 25 лет", 0));
                result.add(new Answer("старше 25 лет", 0));
                break;
            }
            case 21: {
                result.add(new Answer("первый", 0));
                result.add(new Answer("второй", 0));
                result.add(new Answer("третий", 0));
                result.add(new Answer("четвертый", 0));
                result.add(new Answer("пятый", 0));
                break;
            }
            case 22: {
                result.add(new Answer("бакалавриат", 0));
                result.add(new Answer("специалитет", 0));
                result.add(new Answer("магистратура", 0));
                result.add(new Answer("аспирантура", 0));
                break;
            }
            case 23: {
                result.add(new Answer("в общежитии", 0));
                result.add(new Answer("с родителями", 0));
                result.add(new Answer("имею свое жилье", 0));
                result.add(new Answer("снимаю квартиру/комнату", 0));
                break;
            }
            default: {
                result.add(new Answer("полностью удовлетворен", 0));
                result.add(new Answer("частично удовлетворен", 0));
                result.add(new Answer("не удовлетворен", 0));
                result.add(new Answer("затрудняюсь ответить", 0));
            }
        }

    }


}
