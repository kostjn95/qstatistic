Vue.component("question-chart", {
  template:
    '<div class="card my-4" >' +
    '<div class="card-body">' +
    "<h4>{{question.name}}</h4>" +
    '<canvas :id="canvasId" width="1100" height="600"></canvas>' +
    "</div>" +
    "</div>",
  data: function () {
    return {
      myChart: null,
    };
  },
  mounted: function () {
    let labels = this.question.answers.map((v) => v.answer);
    let data = this.question.answers.map((v) => v.count);

    let ctx = document.getElementById(this.canvasId).getContext("2d");
    let myChart = new Chart(ctx, {
      type: "bar",
      data: {
        labels: labels,
        datasets: [
          {
            data: data,
            backgroundColor: [
              "rgba(255, 99, 132, 0.2)",
              "rgba(54, 162, 235, 0.2)",
              "rgba(255, 206, 86, 0.2)",
              "rgba(75, 192, 192, 0.2)",
              "rgba(153, 102, 255, 0.2)",
              "rgba(255, 159, 64, 0.2)",
            ],
            borderColor: [
              "rgba(255, 99, 132, 1)",
              "rgba(54, 162, 235, 1)",
              "rgba(255, 206, 86, 1)",
              "rgba(75, 192, 192, 1)",
              "rgba(153, 102, 255, 1)",
              "rgba(255, 159, 64, 1)",
            ],
            borderWidth: 1,
          },
        ],
      },
      options: {
        responsive: true,
        plugins: {
          legend: {
            display: false,
          },
        },
        scales: {
          y: {
            beginAtZero: true,
          },
        },
      },
    });

    this.myChart = myChart;
  },
  beforeUpdate: function () {
    this.myChart.destroy();
    let labels = this.question.answers.map((v) => v.answer);
    let data = this.question.answers.map((v) => v.count);
    let ctx = document.getElementById(this.canvasId).getContext("2d");
    this.myChart = new Chart(ctx, {
      type: "bar",
      data: {
        labels: labels,
        datasets: [
          {
            data: data,
            backgroundColor: [
              "rgba(255, 99, 132, 0.2)",
              "rgba(54, 162, 235, 0.2)",
              "rgba(255, 206, 86, 0.2)",
              "rgba(75, 192, 192, 0.2)",
              "rgba(153, 102, 255, 0.2)",
              "rgba(255, 159, 64, 0.2)",
            ],
            borderColor: [
              "rgba(255, 99, 132, 1)",
              "rgba(54, 162, 235, 1)",
              "rgba(255, 206, 86, 1)",
              "rgba(75, 192, 192, 1)",
              "rgba(153, 102, 255, 1)",
              "rgba(255, 159, 64, 1)",
            ],
            borderWidth: 1,
          },
        ],
      },
      options: {
        responsive: true,
        plugins: {
            legend: {
                display: false,
            },
        },
        scales: {
            y: {
                beginAtZero: true,
            },
        },
      },
    });
  },
  props: ["question", "ind"],
  computed: {
    canvasId: function () {
      return `chart_${this.ind}`;
    },
  },
});

var app = new Vue({
  el: "#app",
  data() {
    return {
      message: "Hello Vue!",
      activeInstitute: "",
      questions: [],
    };
  },
  methods: {
    checkInstitute: async function (event) {
      let els = document.getElementsByClassName("list-group-item");
      for (el of els) {
        el.classList.remove("active");
      }
      let target = event.target;
      target.classList.toggle("active");

      this.activeInstitute = event.target.childNodes.item(0).nodeValue;

      try {
        const response = await axios.get(
          "/questionnaireResultsByInstitute?institute=" + this.activeInstitute
        );
        this.questions = response.data;
        // console.log(response);
      } catch (error) {
        console.error(error);
      }
    },
  },
});
